﻿using System;
using System.Management;
using System.Windows.Forms;
using System.IO;
using TERClient.TERScanner;
using System.Threading;

namespace TERClient
{
    static class Program
    {

        static void Main()
        {

            new SetClipboardHelper(DataFormats.Text, GetHWID()).Go();

            var processScanner = new ProcessScanner();
            processScanner.scan();

            if (true)
            {
                return;
            }

            Thread fileScanThread = new Thread(FileScan);
            fileScanThread.Start();

            MessageBox.Show("HWID: " + GetHWID());
        }

        static void FileScan()
        {
            
                var fileScanner = new FileScanner();
                fileScanner.scan();

                /*
                while (FileScanner.Running)
                {
                    Thread.Sleep(1000);
                }
                Thread.Sleep(300000);
                */
            
            
        }

        static string GetHWID()
        {
            ManagementObjectCollection managementObjectCollection = null;
            ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_processor");
            managementObjectCollection = managementObjectSearcher.Get();

            string cpuInfo = string.Empty;
            foreach (ManagementObject managementObject in managementObjectCollection)
            {
                cpuInfo += managementObject["ProcessorID"].ToString();
            }

            ManagementObject disk = new ManagementObject(@"win32_logicaldisk.deviceid=""c:""");
            disk.Get();
            string diskInfo = disk["VolumeSerialNumber"].ToString();

            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            ManagementObjectCollection moc = mos.Get();

            string boardInfo = string.Empty;
            foreach (ManagementObject managementObject in moc)
            {
                boardInfo += (string)managementObject["SerialNumber"];
            }

            return TERCrypto.SHA.ComputeSha256Hash(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(cpuInfo + diskInfo + boardInfo)));
        }

    }
}

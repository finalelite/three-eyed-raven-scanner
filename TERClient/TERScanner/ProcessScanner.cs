﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TERClient.TERScanner
{
    class ProcessScanner : Scanner
    {
        public void scan()
        {

            if (Process.GetProcessesByName("javaw").Length == 0)
            {
                return;
            }

            foreach (Process process in Process.GetProcessesByName("javaw"))
            {

                string fileName = Guid.NewGuid().ToString();

                if (Environment.Is64BitOperatingSystem)
                {
                    ExecuteCommandSync("sd64.exe -pid " + process.Id + " -l 4 > " + fileName + ".txt", true);
                }
                else
                {
                    ExecuteCommandSync("sd32.exe -pid " + process.Id + " -l 4 > " + fileName + ".txt", true);
                }


                List<string> strings = new List<string>();
                bool detected = false;
                string detectedString = "null";

                WebClient webClient = new WebClient();
                Stream stream = webClient.OpenRead("http://mc.finalelite.com.br:3000/GCStrings");
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        strings.Add(line);
                    }
                }

                using (FileStream fileStream = File.Open(fileName + ".txt", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (BufferedStream bufferedStream = new BufferedStream(fileStream))
                    {
                        using (StreamReader streamReader = new StreamReader(bufferedStream))
                        {
                            string line;
                            while ((line = streamReader.ReadLine()) != null)
                            {
                                if (strings.Contains(line))
                                {
                                    detected = true;
                                    detectedString = line;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (detected)
                {
                    MessageBox.Show("User is hacking! [" + detectedString + "]");
                }
                else
                {
                    MessageBox.Show("User is not hacking!");
                }


                File.Delete(fileName + ".txt");



            }
        }

        private void ExecuteCommandSync(string command, bool verbose)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd", "/c " + command);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = true;

            Process process = new Process();
            process.StartInfo = processStartInfo;
            process.Start();

            if (verbose)
            {
                Console.WriteLine(process.StandardOutput.ReadToEnd());
            }
        }
    }
}

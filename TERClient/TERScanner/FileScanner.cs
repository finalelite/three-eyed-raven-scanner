﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TERClient.TERScanner
{
    class FileScanner : Scanner
    {

        public static bool Running;

        public void scan()
        {

            Running = true;
            string applicationDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string downloadsFolder = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "{374DE290-123F-4565-9164-39C4925E467B}", String.Empty).ToString();

            string[] extensions = new string[] { "*.exe", "*.jar", "*.rar", "*.zip", "*.json" };

            string[] dotMinecraftFiles = extensions.SelectMany(i => Directory.GetFiles(applicationDataFolder + "/.minecraft", i, SearchOption.AllDirectories)).ToArray();
            string[] downloadFiles = extensions.SelectMany(i => Directory.GetFiles(downloadsFolder, i, SearchOption.AllDirectories)).ToArray();


            Console.WriteLine(".minecraft FILES: ");
            foreach (string file in dotMinecraftFiles)
            {
                
                Console.WriteLine(file + " -> " + TERCrypto.MD_5.CalculateFileMD5(file));
                
            }
            Console.WriteLine("");
            Console.WriteLine("");

            Console.WriteLine("Downloads FILES: ");
            foreach (string file in downloadFiles)
            {
                
                Console.WriteLine(file + " -> " + TERCrypto.MD_5.CalculateFileMD5(file));
                
            }
            Console.WriteLine("");
            Console.WriteLine("");




            Running = false;
        }
    }
}
